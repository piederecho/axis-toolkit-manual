module.exports = function(gulp) {
  var plugins;
  var argv;

  argv = require('yargs').argv;
  plugins = {
    pug     : require('gulp-pug'),
    runSequence: require('run-sequence'),
    htmlmin : require('gulp-htmlmin'),
    rename  : require('gulp-rename'),
    notify  : require('gulp-notify'),
    gulpIf  : require('gulp-if')
  };
  pretty = argv.minify ? false : true;

  function pugTask(src, options, extension) {
    return gulp.src(src, options)
      .pipe(plugins.pug({
        //pretty: !pretty, (PARA COMPRIMIR LOS HTML)
        pretty: pretty,
        data: {
          config: gulp.config
        }
      })
      .on("error",plugins.notify.onError(function (error) {
        return "Error Pug " + error.message;
      })))
      .pipe(plugins.rename(function (path){
          path.extname = extension;
        })
      )
      .on("error",plugins.notify.onError(function (error) {
        console.log('rename');
        return "Error on change extension: " + error.message;
      }))
      .pipe(plugins.gulpIf(!pretty, plugins.htmlmin({
        removeComments    : true,
        collapseWhitespace: true,
        minifyJS          : true
      })
      .on("error",plugins.notify.onError(function (error) {
        return "Error htmlmin: " + error.message;
      }))))
      .pipe(gulp.dest(gulp.config.deploy_routes().templates))
      .pipe(plugins.notify(gulp.config.notifyConfig('Templates compiled')));
  }

  gulp.task('templates:html', function() {
    return pugTask([
        '*.pug',
        '**/*.pug',
        '!_layout.pug',
        '!**/_layout.pug',
        '!includes/**/*.pug',
        '!mixins/**/*.pug',
        '!_*.pug'
      ], {
        cwd : 'templates/sections'
      }, '.html');
  });

  gulp.task('templates:php', function() {
    return pugTask([
        '*.pug',
        '**/*.pug',
        '!_layout.pug',
        '!**/_layout.pug',
        '!includes/**/*.pug',
        '!mixins/**/*.pug',
        '!_*.pug'
      ], {
        cwd : 'templates/php-sections'
      }, '.php');
  });

  gulp.task('templates', function () {
    plugins.runSequence('templates:html', 'templates:php');
  })

  gulp.config.pugTask = pugTask;
}

/*

*/