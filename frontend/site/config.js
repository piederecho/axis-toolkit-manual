var config = {
  deploy_routes : function () {
    var routes = {},
        base,
        static_path;
    if (this.env == 'dev') {
      base = this.settings.root_deploy;
      static_path =  base + '/static';
      routes = {
        base : base,
        templates: base + '/templates',
        static: static_path,
        styles: static_path + '/styles',
        images: static_path + '/images',
        fonts: static_path + '/fonts',
        scripts: static_path + '/scripts',
        sprites: static_path + '/sprites',
        svg: static_path + 'svg/',
      }      
    } else {
      base = this.settings.root_deploy;
      static_path =  base + 'static/';
      routes = {
        base: '',
        templates: base ,
        static: static_path,
        styles: static_path + 'styles/',
        images: static_path + 'images/',
        fonts: static_path + 'fonts/',
        scripts: static_path + 'scripts/',
        sprites: static_path + 'sprites/'
      }
    }
    return routes;
  },
  static_url: function(url) {
    if (this.env == 'prod') {
      return "static/" + url;
    }
    return this.settings.static_uri() + '/' + url;
  },
  notifyConfig : function (options) {
    var _config = {}

    if (typeof options == "object") {
      return options
    }

    _config = {
      message : options,
      onLast  : true
    }

    return _config;
  },
  setEnv : function (env) {
    this.env = env;
  },
  getEnv: function () {
    return this.env;
  },
  notifyConfig : function (options) {
    var _config = {}

    if (typeof options == "object") {
      return options
    }

    _config = {
      message : options,
      onLast  : true
    }

    return _config;
  },
  getSVG: function(name) {
    fs = require('fs');
    return fs.readFileSync('static/svg/' + name + '.svg', 'utf8')
  }
};

module.exports = config;