require([
  'jquery'
  ], function ($) {

    var $doc = $(document);

	function init() {
      dotNavigation();
      switchColorContent();
      switchDataColors();
      closePopup();
      if ($(window).width() < 500 ){
        showDataColorsMobile();  
      }
      if ($(window).width() > 501) {
        showDataColors();
      }
      switchImage();
    }

    function switchImage(){
      $('.navigation__arrows .arrow__right , .navigation__arrows .arrow__left').on('click', function(){
        var dataImage = $(this).data('image');
        var image = `${dataImage}`;
        
        $(this).parent().parent().parent().parent().find('.section__image img').attr('src', image);
      })
    }

    function dotNavigation(){
    	// Show
    	$('.dot__click').on('click', function(){
    		$(this).parent().find('.dot__content').fadeIn();
    	})
    	// Hide
    	$('.dot__content .dot').on('click', function(){
    		$(this).parent().fadeOut();
    	})

    }
    function showDataColorsMobile(){
      $('.dot__click').on('click', function(){
        var sectionShowMobile = $(this).data('mobile');
        var section = $(`.${sectionShowMobile}`);
        section.fadeIn();
        $('.creditos').hide();  
      })
      $('.section__description .close__button').on('click', function(){
        $('.home__sections .section').fadeOut();
        $('.creditos').fadeIn();    
      })
    }
    function showDataColors(){
      $('.dot__content').on('click', function(){
        var sectionShow = $(this).data('show');
        var section = $(`.${sectionShow}`);
        section.fadeIn();
        $('.creditos').hide();  
      })
      $('.section__description .close__button').on('click', function(){
        $('.home__sections .section').fadeOut();
        $('.creditos').fadeIn();    
      })
    }

    function switchColorContent(){
      $('.navigation__arrows .arrow__right , .navigation__arrows .arrow__left').on('click', function(){
        var dataShow = $(this).data('go');
        var data = $(`.${dataShow}`);
        $('.section__description').hide();
        data.fadeIn().css('display', 'inline-block');  
        //$(this).parent().parent().parent().find('section__description').hide();
      })
    }
    function closePopup(){
      $('.popup__close').on('click', function(){
        $('.container__popup').fadeOut();
      })
    }

    function switchDataColors(){
      $('.navigation__dots .dot').on('click', function(){
        var sectionSwitch = $(this).data('switch');
        var section = $(`.${sectionSwitch}`);
        $('.section').fadeOut(900);
        console.log(section);
        section.fadeIn();
        section.find('.section__description').first().show(); // mostar la seccion que ocultamos en la funcion switchColorContent
        $('.creditos').hide();  
      })
      $('.section__description .close__button').on('click', function(){
        $('.home__sections .section').fadeOut();
        $('.creditos').fadeIn();    
      })
    }

	$doc.ready(init);
});