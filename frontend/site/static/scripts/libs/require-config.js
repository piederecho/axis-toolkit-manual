require.config({
  baseUrl:'/static/scripts',
  paths: {
    'alphanumeric': 'libs/jquery.alphanumeric',
    'jquery': 'libs/jquery-1.11.0'
  },
  shim: {
    'alphanumeric':[
      'jquery'
    ]
  }
});